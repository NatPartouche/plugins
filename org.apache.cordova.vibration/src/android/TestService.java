package org.apache.cordova.vibration;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

public class TestService extends CordovaPlugin {

	public TestService(){
		
	}
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		System.out.println("cordova action : "+action);

		if (action.equals("hello")) {
			this.sayhello();
		} else {
			return false;
		}

		// Only alert and confirm are async.
		callbackContext.success();
		return true;
	}

	public void sayhello() {
		System.out.println("ValueHelloFromService");
	}

}
